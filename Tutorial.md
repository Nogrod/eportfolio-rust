# Tutorial

To follow the instructions I recommend to use Inteliji or VSCode. For this you should also install an extension for Rust, which you can find [here](https://www.rust-lang.org/tools).

Alternatively you can also use [rust playground](https://play.rust-lang.org/), there you can write and run Rust code it in your Web Browser.

If you want to learn Rust beyond this Tutorial, I recommend the [rust book](https://doc.rust-lang.org/book/title-page.html).

In the following I presupposed that the basic syntax of Rust is known, this is also briefly discussed in my [presentation](https://docs.google.com/presentation/d/1cFs7ZuEpe6gZEyoMaKqo9CDqyOWNtUvoSRvZfeo-efA/edit?usp=sharing).

## Installation
To follow this tutorial, you will have to have rust installed on your system. A good tutorial on installing go can be found [here](https://doc.rust-lang.org/book/ch01-01-installation.html)

## Cargo
if you rust playground you can skip this part

At first, I want to talk a bit about Cargo, the build and package manager from rust. Cargo get installed together with rust.

To create a project with Cargo type:
`cargo new project_name`

If you show in the created folder "project_name" you will find a file named "Cargo.toml" and a "src" folder. There you find some general information about your project. Like name or version. You can also find the dependencies here, there you can enter all dependencies your project has.


```
[package]
name = "hello_cargo"
version = "0.1.0"
authors = ["Your Name <you@example.com>"]
edition = "2018"

[dependencies]
```

Now we look in the "src" folder. Cargo created there a "main.rs" file. Every Rust file end's with '.rs', also the name should be written as "hello_world.rs". Now look in the "main.rs" file.

```
// filename: src/main.rs
fn main() {
    println!("Hello, world!");
}

```
The first code is `fn main(){`, there we create a new function with the name "main". That "main" function is also special because: it is always the first code that runs in every executable Rust program. I won't explain what a macro is (since it would go beyond the scope here), what is sufficient to know is that you can consider it a normal function call. It is important to mention that macros always end with a "!". That's all that cargo created for us, a "Hello, world" program. 

Now let us build and run our program. You can build it, with the command `Cargo build`, when it is finished you will find an executable in `.\target\debug\hello_cargo.exe`. You can now let this executable run, then you will see "Hello, world!". We just built a project with cargo build and ran it with ./target/debug/hello_cargo, but we can also use `cargo run` to compile the code and then run the resulting executable all in one command.

That should be enough for us, let us code something. 

## Example 1: Ownership / Borrowing Rules
Every code, except for example a function header, must be located in a function. To simplify matters, only the relevant code is shown below.

In the first example, I want to give you an overview of the Ownership and Borrowing rules, there we write some example code. Rusts Ownership concept is important to understand because Ownership enabled Rust to give you Memory safety guarantees. Also, I will talk about Borrowing a related feature to Ownership.

First, let’s take a look at the ownership rules. Keep these rules in mind as we work through the examples that illustrate them:

 - Each value in Rust has a variable that’s called its owner.
 - There can only be one owner at a time.
 - When the owner goes out of scope, the value will be dropped.


Now talk a bit about scope. The scope is the range from a Variable in, within it is Valid. Let's say we have the following code:
```
    {                      // s is not valid here, it’s not yet declared
        let s = 42   // s is valid from this point forward

        // do stuff with s
    }                      // this scope is now over, and s is no longer valid
```
At first, s is not declared, and therefore not valid. In the second line, we declare with 'let' a variable. Often Rust can inferior the type, but if Rust can not do it, we have to do it. The second line would then look like this 'let s: i32 = "hello"'. After it is declared s is valid and can be used. After the last curly bracket in the code example, s goes out of scope and is therefore no longer valid. When a variable goes out of scope Rust will free this memory. This applies even if the memory was used on the heap. 

Now let us look at how we can get in trouble with the Ownership system. 

If we would try the following Code to compile we would get a compile error:
```
    let foo = String::from("Hello");

    let x = foo;
    println!("{} {}", foo, x);
```
In the first line, we declare the new variable 'foo', it holds a string with "Hello". In the next line, we bind the value from foo to x, and in the end we want to print both. The problem is, in the second line 'let x = foo' and that the string is stored on the heap. Because of memory safety issues, Rust moves the Ownership from "foo" to "x" with that foo is invalid and goes out of scope. Thero we can not print "foo" because it is invalid. 

This picture would look different if foo is a for example an integer.
```
    let x = 1;
    let y = x;
    println!("{} {}", x ,y);
```
This example code is fine, compile, and will run without problems. The reason for that is, that "1" is an integer and stored on the stack, therefor Rust will in the second line just copy the value from x. Now we have two distinct Variable where hold the same value and are valid. Therefore we can just print them in the last line.

Both cases are also the same if we would just call a function. as shown in the next example. 
```
fn main() {
    let foo = String::from("Hello");
    take_ownership(foo); // fine
    take_ownership(foo); // Problem
}

fn take_ownership(x: String) {
    // do stuff
}
```
We declare a function named 'take_ownership' with the parameter 'x' which holds a String. When called the function the first time everything is fine but, foo gets moved in the function is not anymore valid. Therefore to call the function a second time with 'foo' is a problem, because it is not valid. We get a compile error.

Similar to the last example, with integer we would not have a problem, because the value gets copied. 

### Borrowing
A related Concept to Ownership is Borrowing. Borrowing is an important concept in Rust, as the name suggests it is also similar to borrowing in the "real world". If you reject something, you give it away and have no access to it yourself. Similarly also in Rust, only here a reference is transferred for the borrowing. As with the ownership concept, we have some rules for borrowing, these are there to ensure that Rust can guarantee memory safety. At any given time, you can have either:
 - one mutable reference or any number of immutable references.
 - References must always be valid.

Now let us look at two examples that violate the borrowing rules and how we can fix it. 

```
fn dangling_pointer() -> & String {
    let foo = String::from("World");
    &foo
}
```
At first, we declare a function that returns a reference to a String. Then we create that String and at the end, we return the reference to the string. That code won't compile because at the end of the function 'foo' goes out of scope and there the reference to it is not valid anymore. This violates a Borrowing rule. If Rust won't catch this error, we would get a reverence to memory that is not valid anymore and it would get a runtime error. To fix it, we could just return the value by itself like shown in the following:

```
fn fix_dangling_pointer() -> String {
    let foo = String::from("World");
    foo
}
```
The ownership is moved out the function and therefore no memory will be freed.
Another example:
```
let mut s = String::from("hello");

let r1 = &s; // no problem
let r2 = &s; // no problem
let r3 = &mut s; // BIG PROBLEM

println!("{}, {}, and {}", r1, r2, r3);
```
In this example, two immutable and one mutable reference exist at the same time. This leads to a Compile error according to the above rules. This problem can be solved as follows.
```
let mut s = String::from("hello");

let r1 = &s; // no problem
let r2 = &s; // no problem
println!("{} and {}", r1, r2);
// r1 and r2 are no longer used after this point

let r3 = &mut s; // no problem
println!("{}", r3);
```
Rust knows that no immutable reference is used after the first console output, so it's okay to create and use a mutable reference after that.

## Example 2: Programing
To become more comfortable with rust, I want to program a little game together. 
We implement a slot machine. So we want to be able to create such a machine and play with it. When you play, three different symbols should be randomly given out and the reward. You get a reward if you get three same symbols in a row.

First, we create a new project with the command 'cargo new slot_machine'. After that, we will write every code in the 'main.rs' file. 
Then we want to illustrate our slot machine. For this, we create a new structure (Put simply, you can say that a structure is similar to a class) "SlotMachine". The slot machine should contain a symbol wheel, which is a vector containing the individual symbols we want to display. 
```
struct SlotMachine {
    symbol_wheel: Vec<Symbol>
}
```
Now we have to create our Symbol struct. This struct should contain the Symbol to display, and the reward if we win with this kind of symbol.

```
struct Symbol {
    icon: char,
    reward: u8,
}
```
After that is taped, we have to create and initialize our slot machine. For this, we want to implement a new method 'new' for the 'slotmachine' struct. This method should not take parameters but return an object of the slotmachine type. Using such a 'new' function is common in Rust.
```
impl SlotMachine {
    fn new() -> SlotMachine {
        let mut wheel = Vec::new();

        for _ in 0..15 {
            wheel.push(Symbol { icon: '🍏', reward: 10 })
        }
        for _ in 0..10 {
            wheel.push(Symbol { icon: '🍋', reward: 25 })
        }
        for _ in 0..5 {
            wheel.push(Symbol { icon: '🔔', reward: 50 })
        }
        wheel.push(Symbol { icon: '💎', reward: 100 });

        SlotMachine { symbol_wheel: wheel }
    }
}
```
Since no parameter with the name self is specified, Rust knows that this is a class method. 

First, we create a new vector in which we want to store our symbols. After that, we create a loop that executes code 15 times. In this loop, we create a new symbol, which in this case we create with an apple and a reward of 10. This symbol is then saved in our vector. So at the end of the loop, we have 15 "apple symbols" on our list. We do the same thing just a few more times, but with a smaller number and a higher reward. So at the end with the diamond we have 31 elements in our vector. Symbols with a lower reward occur more often and symbols with a higher reward occur less often. Finally, we create a new 'SlotMashine' object with our vector, which we return.
With this setup, it is now enough to randomly select three symbols from our list to play the game.

In this sense, we now want to bring a little randomness into our program. Since the standard library of Rust does not provide randomness, we extend our dependencies with the library "rand". For this we extend the "Cargo.toml" under '[dependencies]' with the following line: 'rand = "0.7.3"''. The next time we compile the library will be downloaded automatically. Let's go back to the "main.rs" file. To get random elements from a list, we now have to import two things. 
```
use rand::seq::SliceRandom;
use rand::thread_rng;
```
This code can simply be inserted at the beginning of the file. 

```
use rand::seq::SliceRandom;
use rand::thread_rng;
```
To randomly select an element from the 'symbol_wheel', we implement another method 'choose_random' for the 'SlotMachine'. This method should return a random element from the vector. 
```

impl SlotMachine {
    // --snip--
    
    fn choose(&self) -> &Symbol {
        let mut rng = thread_rng();
        self.symbol_wheel.choose(&mut rng).unwrap()
    }
}
```

Since the method is to be executed on an object, it receives the parameter '&self' and returns a reference to an icon in the list. Due to the import 'rand::seq::SliceRandom' we can now execute the method 'choose' on our vector, which returns a reference to a random element of the vector. However, this reference is in an 'Option' enum, because in case of an empty list 'None' is returned. Since we made sure at initialization that the vector is never empty, we can simply run 'unwrap'. Which gives us the desired content. In addition, the method 'choose' must be passed a 'thread_rng' object, which we create before. This can be used to generate random numbers. We also return our reference automatically.

To do this we want to implement one last method for the 'SlotMachine'. The method 'play'. This method should, when executed, output three random symbols into the console and our reward.

```
impl SlotMachine {
    // --snip--
    fn play(&self) {
        let symbol_1 = self.choose_rand();
        let symbol_2 = self.choose_rand();
        let symbol_3 = self.choose_rand();

        print!("{} {} {}",  symbol_1.icon, symbol_2.icon, symbol_3.icon);
        
        if symbol_1.icon == symbol_2.icon && symbol_3.icon == symbol_2.icon {
            println!(" Reward: {}", symbol_1.reward)
        } else {
            println!(" Reward: {}", 0)
        };
    }
}
```
First, we get our three random symbols. After which we output them to the console. Well noticed without a new line, because we want to spend our reward behind it. Next, we compare our three symbols to see if they are all the same. If they are, we will display our result on the console with the reward of a symbol. If they are different, we give a reward of 0.

Herewith we are finished with the actual game. But to see it still in action we want to adjust our 'main' function.

```
fn main() {
    let machine = SlotMachine::new();

    for _ in 0..10 {
        machine.play();
    }
}
```
First, we create a new 'SlotMachine' to play the game ten times.

Now we can execute the command 'cargo run' to compile and execute our game. 

This output should look something like this:
```
   Compiling slot_machine v0.1.0 (PATH)
    Finished dev [unoptimized + debuginfo] target(s) in 0.62s
     Running `target\debug\slot_machine.exe`
🔔 🍏 🍏 Reward: 0
🍏 🔔 🍋 Reward: 0
🍋 🍋 🍏 Reward: 0
🍏 🍏 🍋 Reward: 0
🍋 🍏 🍋 Reward: 0
🍋 💎 🍏 Reward: 0
🍏 🔔 🍏 Reward: 0
🔔 🍋 🔔 Reward: 0
🍋 🍏 🍏 Reward: 0
🍏 🍏 🍏 Reward: 10

Process finished with exit code 0
```

Herewith the tutorial is finished! The complete code can be found [here](/final_demo/slot_machine).



# e-portfolio: Rust
This repository contains ressources related to the e-portfolio in the software engineering class.  

- [Here](https://docs.google.com/presentation/d/1cFs7ZuEpe6gZEyoMaKqo9CDqyOWNtUvoSRvZfeo-efA/edit?usp=sharing) you find my presenation.
- [final_demo](/final_demo) contains the final source code for the demo.
- [Here](Tutorial.md) you can find the tutorial.


fn main() {
    let foo = String::from("Hello");

    // take_ownership(foo);
    // take_ownership(foo);

    fix_take_ownership(&foo);
    fix_take_ownership(&foo);

    let x = 1;
    copy_value(x);
    copy_value(x);
    // works because 'usize' implement the copy trait.
    // the value is copied and no change is made to ownership from values

    // let foo = dangling_pointer();
    let foo = fix_dangling_pointer();

    // borrowing_rules();
    fix_borrowing_rules()
}

/*
// can just be called once with the same variable, because the owner change
fn take_ownership(x: String) {
    // do stuff
}
*/

fn fix_take_ownership(x: &String){
    // do stuff
}

fn copy_value(x: usize) {
    // do stuff
}

/*
fn dangling_pointer() -> &String {
    let foo = String::from("World");
    // do not compile, because foo get out of scope and the pointer is not valid anymore
    &foo
}
*/


fn fix_dangling_pointer() -> String {
    let foo = String::from("World");
    foo
}

/*
fn borrowing_rules() {
    let mut s = String::from("hello");

    let r1 = &s; // no problem
    let r2 = &s; // no problem
    let r3 = &mut s; // BIG PROBLEM

    println!("{}, {}, and {}", r1, r2, r3);
}
*/

fn fix_borrowing_rules() {
    let mut s = String::from("hello");

    let r1 = &s; // no problem
    let r2 = &s; // no problem
    println!("{} and {}", r1, r2);
    // r1 and r2 are no longer used after this point

    let r3 = &mut s; // no problem
    println!("{}", r3);
}
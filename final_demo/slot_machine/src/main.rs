use rand::seq::SliceRandom;
use rand::thread_rng;


fn main() {
    let machine = SlotMachine::new();

    for _ in 0..10 {
        machine.play();
    }
}



struct SlotMachine {
    symbol_wheel: Vec<Symbol>
}

impl SlotMachine {
    fn new() -> SlotMachine {
        let mut wheel = Vec::new();

        for _ in 0..15 {
            wheel.push(Symbol { icon: '🍏', reward: 10 })
        }
        for _ in 0..10 {
            wheel.push(Symbol { icon: '🍋', reward: 25 })
        }
        for _ in 0..5 {
            wheel.push(Symbol { icon: '🔔', reward: 50 })
        }
        wheel.push(Symbol { icon: '💎', reward: 100 });

        SlotMachine { symbol_wheel: wheel }
    }

    fn play(&self) {
        let symbol_1 = self.choose_rand();
        let symbol_2 = self.choose_rand();
        let symbol_3 = self.choose_rand();

        print!("{} {} {}",  symbol_1.icon, symbol_2.icon, symbol_3.icon);

        if symbol_1.icon == symbol_2.icon && symbol_3.icon == symbol_2.icon {
            println!(" Reward: {}", symbol_1.reward)
        } else {
            println!(" Reward: {}", 0)
        };
    }

    fn choose_rand(&self) -> &Symbol {
        let mut rng = thread_rng();
        self.symbol_wheel.choose(&mut rng).unwrap()
    }
}

struct Symbol {
    icon: char,
    reward: u8,
}

